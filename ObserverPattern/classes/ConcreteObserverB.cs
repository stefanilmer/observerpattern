﻿using ObserverPattern.interfaces;
using System;

namespace ObserverPattern.classes
{
    public class ConcreteObserverB : IObserver
    {
        public string ObserverName = "Observer B";
        public void Update(ISubject subject)
        {
            if ((subject as Subject).State == 0 || (subject as Subject).State >= 2)
            {
                Console.WriteLine("ConcreteObserverB: Reacted to the event.");
            }
        }
        public override string ToString()
        {
            return ObserverName;
        }
    }
}
