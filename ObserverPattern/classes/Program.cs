﻿using ObserverPattern.classes;
using System;

namespace ObserverPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Observer Pattern -  https://refactoring.guru/design-patterns/observer");
            // The client code.
            var subject = new Subject();

            // Attach observer A
            var observerA = new ConcreteObserverA();
            subject.Attach(observerA);

            // Attach observer B
            var observerB = new ConcreteObserverB();
            subject.Attach(observerB);

            // Do something with observers
            subject.SomeBusinessLogic();
            subject.SomeBusinessLogic();

            // See who is observing
            subject.WhoIsObserving();

            // Detach observer A
            subject.Detach(observerA);

            // See who is observing
            subject.WhoIsObserving();

            // Detach observer B
            subject.Detach(observerB);

            // Do something with observers
            subject.SomeBusinessLogic();

            // See who is observing
            subject.WhoIsObserving();
        }
    }
}
