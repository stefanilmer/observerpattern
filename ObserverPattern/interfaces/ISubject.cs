﻿namespace ObserverPattern.interfaces
{
    public interface ISubject
    {
        // Attach an observer to the subject
        void Attach(IObserver observer);

        // Detatch an observer from the subject
        void Detach(IObserver observer);

        // Notify all observers about an event
        void Notify();

        // Shows who is observing
        void WhoIsObserving();
    }
}
