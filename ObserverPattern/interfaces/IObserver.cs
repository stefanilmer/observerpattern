﻿namespace ObserverPattern.interfaces
{
    public interface IObserver
    {
        // receive update from subject
        void Update(ISubject subject);
    }
}
